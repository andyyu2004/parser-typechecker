use crate::parsing::{Span, Binder, Expr, ExprKind};
use crate::typechecking::{Ty, TyKind};
use regexlexer::Token;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct Error {
    pub span: Span,
    pub msg: String,
}

impl PartialEq for Error { fn eq(&self, other: &Self) -> bool { self.msg == other.msg } }

impl Error {
    pub fn new(span: Span, msg: String) -> Self {
        Error { span, msg }
    }

    pub fn invalid_else_expression(span: Span, token: Token) -> Self {
        Self::new(span, format!("invalid start of else expression `{}`\n expected block or if expression", token))
    }

    pub fn cannot_elide_item_type_annotations(span: Span, name: &str) -> Self {
        Self::new(span, format!("cannot elide type annotation in item definitions; `{}` has no type annotation", name))
    }

    pub fn unification_failure(span: Span, t: &TyKind, u: &TyKind) -> Self {
        Error::new(span, format!("Failed to unify type `{}` with `{}`", t, u))
    }

    pub fn require_main_function() -> Self {
        Error::new(Span::new(0, 0, 1), "fn main could not be found".to_owned())
    }

    pub fn invalid_main_function_ty(ty: &Ty) -> Self {
        Error::new(Span::new(0, 0, 1), format!("fn main must have type signature `fn () -> T for some type T`, but found `{}`", ty))
    }

    pub fn expected_lvalue(found: &Expr) -> Self {
        Error::new(found.span, format!("expected lvalue as assignment target, found rvalue {}", found))
    }

    pub fn reassignment_to_immutable_binding(span: Span, name: &str) -> Self {
        Error::new(span, format!("cannot reassign to immutable binding `{}`", name))
    }

    pub fn unbound_variable(span: Span, name: &str) -> Self {
        Error::new(span, format!("could not find variable `{}` in scope", name))
    }

    pub fn invalid_item(span: Span, token: Token) -> Self {
        Error::new(span, format!("invalid item keyword, found `{}`. expected one of fn, struct, use", token))
    }

    pub fn read_file_failed(span: Span, msg: &str, path: &str) -> Self {
        Error::new(span, format!("failed to read file {}. {}", path, msg))
    }

    pub fn undefined_type(span: Span, ty: &str) -> Self {
        Self::new(span, format!("undefined type `{}`", ty))
    }

    pub fn property_access_on_non_struct(span: Span, kind: &TyKind) -> Self {
        Self::new(span, format!("cannot access property on non struct type `{}`", kind))
    }

    pub fn non_existent_field(expr: &Expr, field: &str, fields: &HashMap<String, Ty>) -> Self {
        Self::new(expr.span, format!("no field `{}` on `{}` which has fields {:?}", field, expr, fields))
    }


}
