
pub fn join<'a, T : 'a>(xs: impl IntoIterator<Item = &'a T>, sep: &str) -> String where T : std::fmt::Display {
    xs.into_iter().map(|x| x.to_string()).collect::<Vec<String>>().join(sep)
}

pub fn join2<T>(xs: impl IntoIterator<Item = T>, sep: &str) -> String where T : std::fmt::Display {
    xs.into_iter().map(|x| x.to_string()).collect::<Vec<String>>().join(sep)
}
