use crate::typechecking::TyKind;

#[macro_export]
macro_rules! tuple {
    ( $($e:expr),* ) => {
        {
            let v = vec![$($e),*];
            TyKind::Tuple(v)
        }
    }
}
