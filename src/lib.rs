#![feature(box_syntax, box_patterns)]
#![cfg_attr(feature = "cargo-clippy", allow(clippy::trivial_regex))]

#[macro_use] extern crate colour;

mod parsing;
mod lexing;
mod error;
mod util;
mod macros;
mod typechecking;
mod hir;

use regexlexer::{Lexer, LexSyntax};
use lexing::gen_syntax;
use typechecking::Typechecker;
use util::Counter;
use std::collections::HashMap;

pub use error::{Error, Formatter};
pub use parsing::*;
pub use regexlexer::{Token, TokenKind};
pub use typechecking::{Ty, TyKind};
pub use std::fs;


/// Generate ast using the default syntax provided from this crate
pub fn generate_ast(src: &str) -> Result<(Ty, Expr), Vec<Error>> {
    generate_ast_with_syntax(src, &gen_syntax())
}

pub fn parse_program(src: &str) -> (Vec<Ty>, Vec<Item>) {
    parse_program_result(src).unwrap_or_else(|errors| {
        let formatter = Formatter::new(src);
        formatter.write(errors);
        std::process::exit(1);
    })
}

// return types of all the items in the program
fn use_file(path: &str, span: Span) -> Result<HashMap<String, Ty>, Vec<Error>> {
    let src = fs::read_to_string("/home/andy/programming/rust/parser-typechecker/lib.l")
        .or_else(|err| Err(Error::read_file_failed(span, &format!("{}", err), path)))
        .map_err(|err| vec![err])?;
    let (tys, items) = parse_program_result(&src)?;
    Ok(items.into_iter()
        .map(|item| item.name)
        .zip(tys)
        .collect())
}

fn parse_program_result(src: &str) -> Result<(Vec<Ty>, Vec<Item>), Vec<Error>> {
    let syntax = gen_syntax();
    let lexer = Lexer::new(src, &syntax);
    let tokens = match lexer.lex() {
        Ok(ts) => ts,
        Err(errors) => {
            eprintln!("{}", errors.join("\n"));
            std::process::exit(1);
        }
    };

    let mut gen = Counter::new();
    let mut parser = Parser::new(&tokens, &mut gen);
    let mut prog = parser.parse_program()?;
    let mut typechecker = Typechecker::new(&mut gen);
    let tys = typechecker.typecheck(&mut prog)?;

    Ok((tys, prog))
}

pub fn generate_ast_with_syntax(src: &str, syntax: &LexSyntax) -> Result<(Ty, Expr), Vec<Error>> {
    let lexer = Lexer::new(src, &syntax);
    let tokens = match lexer.lex() {
        Ok(ts) => ts,
        Err(errors) => {
            eprintln!("{}", errors.join("\n"));
            std::process::exit(1);
        }
    };

    let mut gen = Counter::new();
    let mut parser = Parser::new(&tokens, &mut gen);
    let mut expr = parser.run_parse_expr()?;
    let mut typechecker = Typechecker::new(&mut gen);
    let ty = typechecker.typecheck_expr(&mut expr)?;

    Ok((ty, expr))
}

pub fn force_gen_ast_no_typecheck(src: &str) -> Expr {
    let syntax = gen_syntax();
    let lexer = Lexer::new(src, &syntax);
    let tokens = lexer.lex().unwrap();
    let mut gen = Counter::new();
    let mut parser = Parser::new(&tokens, &mut gen);
    parser.run_parse_expr().unwrap()
}

pub fn generate_ast_with_err_handling(src: &str) -> (Ty, Expr) {
    let syntax = gen_syntax();
    let lexer = Lexer::new(src, &syntax);
    let tokens = match lexer.lex() {
        Ok(ts) => ts,
        Err(errors) => {
            eprintln!("{}", errors.join("\n"));
            std::process::exit(1);
        }
    };

    // println!("{:?}", tokens);
    let mut gen = Counter::new();
    let mut parser = Parser::new(&tokens, &mut gen);
    let formatter = Formatter::new(src);

    let mut ast = match parser.run_parse_expr() {
        Ok(ast) => ast,
        Err(errors) => {
            formatter.write(errors);
            std::process::exit(1);
        }
    };

    let mut typechecker = Typechecker::new(&mut gen);
    let ty = match typechecker.typecheck_expr(&mut ast) {
        Ok(ast) => ast,
        Err(errors) => {
            formatter.write(errors);
            std::process::exit(1);
        }
    };

    (ty, ast)
}




#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_counter() {
        let mut gen = Counter::new();
        assert_eq!(1, gen.next());
        assert_eq!(2, gen.next());
        assert_eq!(3, gen.next());
        assert_eq!(4, gen.next());
    }
}














