use super::{TyScheme, Type, Substitution};
use std::collections::HashSet;

/// entry into the ty_env
pub(crate) struct Entry {
    pub mutable: bool,
    pub ty_scheme: TyScheme,
}

impl Entry {
    pub fn new(mutable: bool, ty_scheme: TyScheme) -> Self {
        Self { mutable, ty_scheme }
    }
}

impl Type for Entry {
    fn apply(&mut self, s: &Substitution) {
        self.ty_scheme.apply(s)
    }

    fn ftv(&self) -> HashSet<u64> {
        self.ty_scheme.ftv()
    }
}

impl From<TyScheme> for Entry {
    fn from(ty_scheme: TyScheme) -> Self {
        Self { mutable: false, ty_scheme }
    }
}



