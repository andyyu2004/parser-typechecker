use crate::parsing::{Expr, ExprKind, Span, BinOp, Item, ItemKind, Binder};
use crate::error::Error;
use super::{TyKind, Ty, Env, Constraint, Type, TyScheme, Entry, Ctx, Substitution, solve};
use crate::util::{self, Counter};
use crate::arrow;
use std::collections::HashSet;

pub struct Typechecker<'a> {
    pub(crate) env: Env<String, TyScheme>, // scoped map :: String -> TyScheme at value level
    pub(crate) type_env: Ctx<String, TyScheme>, // map :: String -> TyScheme at type level
    pub(crate) name_gen: &'a mut Counter,
}

impl<'a> Typechecker<'a> {
    pub fn new(name_gen: &'a mut Counter) -> Self {
        Self { env: Env::new(), type_env: Ctx::new(), name_gen }
    }

    /// items must all have full type signatures
    /// declare all signatures before processing the internals
    /// this allows for hoisting all items
    pub fn typecheck(&mut self, program: &mut Vec<Item>) -> Result<Vec<Ty>, Vec<Error>> {
        let mut types = vec![];
        let mut constraints = vec![];
        let mut errors = vec![];

        // declare all items in type env
        for item in program.iter_mut() {
            match &mut item.kind {
                ItemKind::Use { path } => {
                    // adds all the items defined in the src file at path into the type env
                    match crate::use_file(&path, item.span) {
                        Ok(ctx)   => self.env.extend(ctx),
                        Err(errs) => errors.extend(errs),
                    }
                }
                ItemKind::Fn { params, ret_ty, .. } => {
                    let tparams = params.iter().map(|binder| binder.ty.clone()).collect::<Vec<Ty>>();
                    let ty_params = Ty::new(item.span, TyKind::Tuple(tparams));
                    let mut fn_ty = Ty::new(item.span, TyKind::Arrow(box ty_params, box ret_ty.clone()));
                    // current implementation implicitly universally quantifies over all type variables without using the specified type parameters
                    let substitutions = fn_ty.lower_tyvar_to_uvar(self.name_gen);
                    let forall = substitutions.values().cloned().collect::<HashSet<u64>>();
                    let ty_scheme = TyScheme::new(fn_ty, forall);
                    self.define_var(item.name.clone(), ty_scheme, false).unwrap_or_else(|err| errors.push(err));
                }
                ItemKind::Struct { fields } => {
                    // TODO generics not implemented
                    fields.values_mut().for_each(|t| { t.lower_tyvar_to_uvar(self.name_gen); });
                    let ty_scheme = TyScheme::from(Ty::new(item.span, TyKind::Struct(fields.clone())));
                    self.type_env.insert(item.name.to_owned(), ty_scheme);
                },
            }
        };

        // replace all type names with the type
        program.iter_mut().
            map(|item| item.ty.substitute_tynames(self))
            .collect::<Result<Vec<_>, _>>()
            .map_err(|err| vec![err])?;

        for item in program {
            match self.typecheck_item(item) {
                Ok((ty, constraint)) => {
                    types.push(ty);
                    constraints.push(constraint);
                }
                Err(err) => errors.push(err),
            }
        }

        if let Some(ty_scheme) = self.env.lookup("main") {
            if ty_scheme.ty.kind.left_of_arrow().kind != TyKind::unit() { errors.push(Error::invalid_main_function_ty(&ty_scheme.ty)) }
        } else { errors.push(Error::require_main_function()) }

        let c = Constraint::conj(constraints);
        let substitution = solve(c).map_err(|err| vec![err])?;
        types.iter_mut().for_each(|t| t.apply(&substitution));

        if errors.is_empty() { Ok(types) } else { Err(errors) }
    }


    /// adds binding to env; substitutes typenames with actual type
    fn define_var(&mut self, var: String, mut scheme: TyScheme, mutable: bool) -> Result<(), Error> {
        scheme.ty.substitute_tynames(self)?;
        scheme.ty.mutable = mutable;
        Ok(self.env.define(var, scheme))
    }

    /// does the actual typechecking of the bodies etc...
    pub fn typecheck_item(&mut self, item: &mut Item) -> Result<(Ty, Constraint), Error> {
        match &mut item.kind {
            // is the additional constraint that the type is eq to the one defined in env necessary?
            ItemKind::Fn { params, ret_ty, body } => self.typecheck_lambda(item.span, &mut item.ty, params, ret_ty, body),
            ItemKind::Use { .. } => Ok((Ty::new(item.span, TyKind::unit()), Constraint::Empty)),
            ItemKind::Struct { .. } => Ok((Ty::new(item.span, TyKind::unit()), Constraint::Empty)),
        }
    }

    pub fn typecheck_expr(&mut self, expr: &mut Expr) -> Result<Ty, Vec<Error>> {
        let (mut t, c) = self.infer(expr).map_err(|e| vec![e])?;
        println!("c: {}", c);
        let substitution = solve(c).map_err(|err| vec![err])?;
        t.apply(&substitution);
        Normalizer::new().normalize(&mut t.kind);
        Ok(t)
    }

    pub fn infer(&mut self, expr: &mut Expr) -> Result<(Ty, Constraint), Error> {
        match &mut expr.kind {
            ExprKind::Id { name } => match self.env.lookup(name) {
                Some(ty_scheme) => Ok((ty_scheme.instantiate(self.name_gen), Constraint::Empty)),
                None => Err(Error::new(expr.span, format!("Unbound variable `{}`", name)))
            }
            ExprKind::Let { binder, bound, mutable } => {
                self.env.push();
                let (tbound, cbound) = self.infer(bound)?;
                let c_tbound_eq_binder_annotation = Constraint::Eq(tbound.clone(), binder.ty.clone());
                let c_texpr_eq_tbound = Constraint::Eq(tbound.clone(), expr.ty.clone());
                let cs = Constraint::conj(vec![
                    cbound,
                    c_tbound_eq_binder_annotation,
                    c_texpr_eq_tbound
                ]);
                let s = solve(cs.clone())?;
                self.env.apply(&s); // must apply this principle substitution to env as well as the type
                let mut principle_ty = tbound;
                principle_ty.apply(&s);
                let generalized = principle_ty.generalize(&self.env);
                self.define_var(binder.name.clone(), generalized, *mutable)?;
                let tret = Ty::new(expr.span, TyKind::unit()); // Let expressions always return unit;
                Ok((tret, cs))
            }
            ExprKind::Lambda { params, ret, body } => self.typecheck_lambda(expr.span, &mut expr.ty, params, ret, body),
            ExprKind::App { f, args } => {
                let fspan = f.span; // for borrow checker reasons
                let (tf, cf) = self.infer(f)?;
                let xs = args.iter_mut().map(|e| self.infer(e)).collect::<Result<Vec<_>, _>>()?;
                let (vargs, mut cargs) = util::split(xs);
                let targs = box Ty::new(expr.span, TyKind::Tuple(vargs));
                let capp = Constraint::Eq(tf, Ty::new(fspan, TyKind::Arrow(targs, box expr.ty.clone())));
                cargs.extend(vec![cf, capp]);
                let cs = Constraint::conj(cargs);
                Ok((expr.ty.clone(), cs))
            }
            ExprKind::Block { exprs, suppressed } => {
                self.env.save();
                self.env.push();
                let xs = exprs.iter_mut().map(|e| self.infer(e)).collect::<Result<Vec<_>, _>>()?;
                let (mut types, constraints) = util::split(xs);
                let block_type = if *suppressed { Ty::new(expr.span, TyKind::unit()) } else { types.remove(types.len() - 1) };
                self.env.restore();
                Ok((block_type, Constraint::conj(constraints)))
            }
            ExprKind::Tuple { elems } => {
                let xs = elems.iter_mut().map(|e| self.infer(e)).collect::<Result<Vec<_>, _>>()?;
                let (types, constraints) = util::split(xs);
                let ty = Ty::new(expr.span, TyKind::Tuple(types));
                Ok((ty, Constraint::conj(constraints)))
            }
            ExprKind::If { cond, left, right } => {
                let (tcond, ccond) = self.infer(cond)?;
                let (tleft, cleft) = self.infer(left)?;
                let (tright, cright) = if let Some(r) = right { self.infer(r)? }
                else { (Ty::new(expr.span, TyKind::unit()), Constraint::Empty) };
                let cs = vec! [
                    Constraint::Eq(tleft, tright.clone()), // if no else branch, then branch must return unit
                    Constraint::Eq(Ty::new(tcond.span, TyKind::Bool), tcond),
                    ccond, cleft, cright
                ];
                Ok((tright, Constraint::conj(cs)))
            }
            ExprKind::Lazy { left, right, .. } => {
                let lspan = left.span;
                let rspan = right.span;
                let (tleft, cleft) = self.infer(left)?;
                let (tright, cright) = self.infer(right)?;
                let tret = Ty::new(expr.span, TyKind::Bool);
                Ok((tret, Constraint::conj(vec![
                    cleft,
                    cright,
                    Constraint::Eq(tleft, Ty::new(lspan, TyKind::Bool)),
                    Constraint::Eq(tright, Ty::new(rspan, TyKind::Bool)),
                ])))
            }
            ExprKind::While { box cond, box body } => {
                let cond_span = cond.span;
                let (tcond, ccond) = self.infer(cond)?;
                let (_tbody, cbody) = self.infer(body)?;
                Ok((Ty::new(expr.span, TyKind::unit()), Constraint::conj(vec![
                    ccond,
                    cbody,
                    Constraint::Eq(tcond, Ty::new(cond_span, TyKind::Bool))
                ])))
            }
            ExprKind::Assn { name, expr } => {
                match self.env.lookup(name) {
                    None => Err(Error::unbound_variable(expr.span, name)),
                    Some(ty_scheme) => {
                        if !ty_scheme.ty.mutable { return Err(Error::reassignment_to_immutable_binding(expr.span, name)) }
                        let var_ty = ty_scheme.instantiate(&mut self.name_gen);
                        let (texpr, cexpr) = self.infer(expr)?;
                        let c_tyvar_eq_texpr = box Constraint::Eq(var_ty, texpr.clone());
                        let c = Constraint::And(box cexpr, c_tyvar_eq_texpr);
                        Ok((texpr, c))
                    }
                }
            }
            ExprKind::Struct { name, fields } => {
                let struct_scheme = if let Some(t) = self.type_env.lookup(name) { t }
                else { return Err(Error::undefined_type(expr.span, &name)) };
                let instantiated = struct_scheme.instantiate(self.name_gen);

                let (names, ty_constraints_pairs) = util::split(fields.iter_mut().map(|(n, x)| (n.to_owned(), self.infer(x))).collect::<Vec<_>>());
                let (tys, mut cs) = util::split(ty_constraints_pairs.into_iter().collect::<Result<Vec<_>,_>>()?);
                let struct_ty = Ty::new(expr.span, TyKind::Struct(names.into_iter().zip(tys).collect()));
                cs.push(Constraint::Eq(struct_ty, instantiated));
                // once done interior check; just return the typename
                Ok((expr.ty.clone(), Constraint::conj(cs)))
            }
            ExprKind::Get { expr, field } => {
                let (texpr, cexpr) = self.infer(expr)?;
                match &texpr.kind {
                    TyKind::Struct(fields) => match fields.get(field) {
                        Some(ty) => Ok((ty.clone(), cexpr)),
                        None     => Err(Error::non_existent_field(&expr, field, fields)),
                    }
                    kind => Err(Error::property_access_on_non_struct(expr.span, &kind))
                }
            }
            ExprKind::Binary { op, box left, box right } => {
                match op {
                    // restrict arithmetic to integral for now
                    BinOp::Sub
                        | BinOp::Mul
                        | BinOp::BitAnd
                        | BinOp::BitOr
                        | BinOp::Div
                        | BinOp::Add => {
                            let lspan = left.span;
                            let rspan = right.span;
                            let (tleft, cleft) = self.infer(left)?;
                            let (tright, cright) = self.infer(right)?;
                            let tret = Ty::new(expr.span, TyKind::I64);
                            Ok((tret, Constraint::conj(vec![
                                cleft, cright,
                                Constraint::Eq(tleft, Ty::new(lspan, TyKind::I64)),
                                Constraint::Eq(tright, Ty::new(rspan, TyKind::I64)),
                            ])))
                        }
                    // restrict cmp to integers for now
                    BinOp::LT
                        | BinOp::LTE
                        | BinOp::GT
                        | BinOp::GTE => {
                            let lspan = left.span;
                            let rspan = right.span;
                            let (tleft, cleft) = self.infer(left)?;
                            let (tright, cright) = self.infer(right)?;
                            let tret = Ty::new(expr.span, TyKind::Bool);
                            Ok((tret, Constraint::conj(vec![
                                cleft, cright,
                                Constraint::Eq(tleft, Ty::new(lspan, TyKind::I64)),
                                Constraint::Eq(tright, Ty::new(rspan, TyKind::I64)),
                            ])))
                        }
                    _ => unimplemented!()
                }
            }
            ExprKind::Grouping { expr } => self.infer(expr),
            k@ExprKind::Bool { .. } | k@ExprKind::Integral { .. } => Ok(Self::typecheck_literal(k, &expr.ty, expr.span)),
            expr => unimplemented!("{}", expr),
        }
    }

    fn typecheck_lambda(&mut self, span: Span, expr_ty: &mut Ty, params: &mut Vec<Binder>, ret_ty: &Ty, body: &mut Expr) -> Result<(Ty, Constraint), Error> {
        self.env.push();
        let tparams = Ty::new(span, TyKind::Tuple(params.iter_mut().map(|binder| {
            self.define_var(binder.name.clone(), TyScheme::from(binder.ty.clone()), true)?;
            Ok(binder.ty.clone())
        }).collect::<Result<Vec<_>, _>>()?));

        let (tbody, cbody) = self.infer(body)?;
        let tlambda = Ty::new(span, TyKind::Arrow(box tparams, box tbody.clone()));
        let clambda = Constraint::Eq(tlambda.clone(), expr_ty.clone());
        let c_ret_eq_body = Constraint::Eq(tbody, ret_ty.clone());
        let cs = Constraint::conj(vec![clambda, c_ret_eq_body, cbody]);

        self.env.pop();
        Ok((tlambda, cs))
    }

    fn typecheck_literal(exprkind: &ExprKind, ty: &Ty, span: Span) -> (Ty, Constraint) {
        debug_assert_eq!(ty.kind, Self::type_of_literal_expr(exprkind));
        (Ty::new(span, Self::type_of_literal_expr(exprkind)), Constraint::Empty)
    }

    fn type_of_literal_expr(exprkind: &ExprKind) -> TyKind {
        match exprkind {
            ExprKind::Integral { .. } => TyKind::I64,
            ExprKind::Bool { .. }     => TyKind::Bool,
            _ => panic!("{} is not a literal", exprkind)
        }
    }

}

use variable_gen::Generator;
use std::collections::HashMap;

/// simplfiies type names
struct Normalizer {
    name_gen: Generator,
    names: HashMap<u64, String>,
}

impl Normalizer {

    pub fn new() -> Self {
        Self { name_gen: Generator::new(), names: HashMap::new() }
    }

    pub fn normalize(&mut self, ty: &mut TyKind) {
        match ty {
            TyKind::Infer(i) => match self.names.get(i) {
                Some(name) => *ty = TyKind::TyVar(name.clone()),
                None => {
                    let new_name = self.name_gen.gen();
                    self.names.insert(*i, new_name.clone());
                    *ty = TyKind::TyVar(new_name);
                }
            }
            TyKind::TyVar(_) => {}
            _ => ty.apply_transformation(|ty| self.normalize(ty))
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{arrow, tuple};

    macro_rules! typecheck { ($src:expr) => { { crate::generate_ast($src).unwrap().0 } } }


    #[test] fn typeof_int() { assert_eq!(typecheck!("5"), TyKind::I64.to_ty()) }
    #[test] fn typeof_bool() { assert_eq!(typecheck!("false"), TyKind::Bool.to_ty()) }

    #[test]
    fn typeof_tuple() {
        assert_eq!(
            typecheck!("(1, false)"),
            // (i64, bool)
            TyKind::Tuple(vec![
                TyKind::I64.to_ty(),
                TyKind::Bool.to_ty(),
            ]).to_ty()
        )
    }

    #[test]
    fn typeof_id_fn() {
        assert_eq!(
            typecheck!("fn x: Int => x"),
            // (i64) -> i64
            arrow!(TyKind::I64.to_ty().singleton() => TyKind::I64.to_ty())
        )
    }

    #[test]
    fn typeof_simple_fn() {
        assert_eq!(
            typecheck!("fn x: Int => false"),
            // (i64) -> bool
            arrow!(TyKind::I64.to_ty().singleton() => TyKind::Bool.to_ty())
        )
    }

    #[test]
    fn typeof_uncurry() {
        let t = typecheck!("fn x => fn y => (x, y)");
        // (a) -> (b) -> (a, b)
        let expected = arrow!(
            TyKind::TyVar("a".to_owned()).to_ty().singleton() =>
            TyKind::TyVar("b".to_owned()).to_ty().singleton() =>
            TyKind::Tuple(vec![
                TyKind::TyVar("a".to_owned()).to_ty(),
                TyKind::TyVar("b".to_owned()).to_ty(),
            ]).to_ty()
        );
        assert_eq!(t, expected);
    }

    #[test]
    fn typeof_double_application() {
        let t = typecheck!("fn f => fn x => f(f(x))");
        // ((a) -> a) -> (a) -> a
        let tf = arrow!(TyKind::TyVar("a".to_owned()).to_ty().singleton() => TyKind::TyVar("a".to_owned()).to_ty()).singleton();
        let expected = arrow!(tf => TyKind::TyVar("a".to_owned()).to_ty().singleton() => TyKind::TyVar("a".to_owned()).to_ty());
        assert_eq!(t, expected)
    }

    #[test]
    fn test_generalization_of_let() {
        // testing against over generalization; where y maybe inferred to be polymorphic
        let t = typecheck!("(fn x => { let y = x; y })(3)");
        assert_eq!(t, TyKind::I64.to_ty());
        let t = typecheck!("(fn x => { let y = x; let t = y;  t})(false)");
        assert_eq!(t, TyKind::Bool.to_ty());
    }

    #[test]
    fn test_generalization_of_fn() {
        let src = "fn f(x: 'a, y: 'a) -> Int { 5 }
                   fn test() -> Int { f(1, false) }";
        let es = crate::parse_program_result(src).unwrap_err();
        assert_eq!(es, vec![Error::unification_failure(Span::default(), &TyKind::I64, &TyKind::Bool)]);
    }

    #[test]
    fn test_function() {
        let src = "fn f(x: 'a, y: 'b) -> Int { 5 }
                   fn test() -> Int { f(1, false) }
                   fn main() {}";
        let ts = crate::parse_program_result(src).unwrap().0;
        assert_eq!(ts, vec![
            arrow! { tuple!(TyKind::TyVar("'a".to_owned()).to_ty(), TyKind::TyVar("'b".to_owned()).to_ty()).to_ty() => TyKind::I64.to_ty() },
            arrow! { tuple!().to_ty() => TyKind::I64.to_ty() },
            arrow! { tuple!().to_ty() => tuple!().to_ty() },
        ]);
    }

    #[test]
    fn test_struct() {
        let src = "struct S { x: Int; y: Bool }
                   fn main() { let x: S = S { x: 3, y: 4} }";
        let errs = crate::parse_program_result(src).unwrap_err();
        assert_eq!(errs, vec![Error::unification_failure(Span::default(), &TyKind::I64, &TyKind::Bool)])
    }

    #[test]
    fn test_struct_field_access() {
        let src = "struct S { x: Int; y: Bool }
                   fn main() -> Bool { let x: S = S { x: 3, y: false }; x.y }";
        let ts = crate::parse_program_result(src).unwrap().0;
        assert_eq!(ts, vec![
            TyKind::unit().to_ty(),
            arrow! { TyKind::unit().to_ty() => TyKind::Bool.to_ty() },
        ]);
    }


}








