use std::fmt::{self, Display, Formatter, Debug};
use super::{Type, Substitution, TyScheme, Typechecker};
use std::collections::{HashSet, HashMap};
use crate::{arrow, set};
use crate::parsing::{Span, fmt_iter_debug};
use crate::util::Counter;
use crate::error::Error;

#[derive(Clone, Eq)]
pub struct Ty {
    pub span: Span,
    pub mutable: bool,
    pub kind: TyKind,
}

impl PartialEq for Ty {
    fn eq(&self, other: &Self) -> bool { self.kind == other.kind }
}

impl Ty {
    pub fn new(span: Span, kind: TyKind) -> Self {
        Self { span, kind, mutable: false }
    }

    pub fn new_mut(span: Span, kind: TyKind) -> Self {
        Self { span, kind, mutable: true }
    }

    pub(crate) fn generalize(self, env: &impl Type) -> TyScheme {
        let forall = &self.ftv() - &env.ftv();
        TyScheme::new(self, forall)
    }

    /// wraps Ty into singleton tuple
    pub(crate) fn singleton(self) -> Self {
        let span = self.span;
        Self::new(span, TyKind::Tuple(vec![self]))
    }

    /// replaces the String based TyVar with u64 uvars
    /// this function already applies the transformation but also returns the map
    pub(crate) fn lower_tyvar_to_uvar(&mut self, name_gen: &mut Counter) -> HashMap<String, u64> { self.kind.lower_tyvar_to_uvar(name_gen) }


    pub(crate) fn substitute_tynames(&mut self, typechecker: &mut Typechecker) -> Result<(), Error> {
        match &mut self.kind {
            TyKind::TyName(name) => match typechecker.type_env.lookup(name) {
                Some(scheme) => {
                    let prev_span = self.span;
                    *self = scheme.instantiate(typechecker.name_gen);
                    self.span = prev_span;
                    Ok(())
                },
                None => Err(Error::undefined_type(self.span, name))
            },
            TyKind::Tuple(ts) => ts.iter_mut().map(|t| t.substitute_tynames(typechecker)).collect(),
            TyKind::Arrow(box l, box r) => {
                l.substitute_tynames(typechecker)?;
                r.substitute_tynames(typechecker)
            }
            TyKind::Struct(fields) => fields.values_mut().map(|t| t.substitute_tynames(typechecker)).collect(),
            _ => Ok(())
        }
    }

}

impl Type for Ty {
    fn apply(&mut self, s: &Substitution) { self.kind.apply(s) }
    fn ftv(&self) -> HashSet<u64> { self.kind.ftv() }
}

impl Display for Ty { fn fmt(&self, f: &mut Formatter) -> fmt::Result { write!(f, "{}", self.kind) } }
impl Debug for Ty { fn fmt(&self, f: &mut Formatter) -> fmt::Result { write!(f, "{}", self) } }

#[derive(Clone, PartialEq, Debug, Eq)]
pub enum TyKind {
    Erased,
    Bool,
    I64,
    F64,
    Infer(u64), // Unification type variable
    TyVar(String),
    Tuple(Vec<Ty>),
    Arrow(Box<Ty>, Box<Ty>),
    Struct(HashMap<String, Ty>),
    TyName(String),
}

impl TyKind {
    pub fn unit() -> Self { Self::Tuple(Vec::new()) }

    /// generates a substitution :: String -> u64, and applies and returns it
    pub(crate) fn lower_tyvar_to_uvar(&mut self, name_gen: &mut Counter) -> HashMap<String, u64> {
        let mut map = HashMap::new();
        self.lower_tyvar_to_uvar_helper(name_gen, &mut map);
        map
    }

    fn lower_tyvar_to_uvar_helper(&mut self, name_gen: &mut Counter, map: &mut HashMap<String, u64>) {
        match self {
            Self::TyVar(name) => {
                let i = if let Some(&i) = map.get(name) { i } else {
                    let new_name = name_gen.next();
                    map.insert(name.clone(), new_name);
                    new_name
                };
                *self = TyKind::Infer(i);
            }
            Self::Infer(_) => {},
            _ => self.apply_transformation(|ty| ty.lower_tyvar_to_uvar_helper(name_gen, map))
        }
    }

    pub(crate) fn left_of_arrow(&self) -> &Ty {
        match self {
            Self::Arrow(box l, box _r) => l,
            _ => panic!("expected arrow found {}", self)
        }
    }

    /// this function generalizes propogation and requires only the actions to be performed on tyvar and infer case
    pub(crate) fn apply_transformation(&mut self, mut f: impl FnMut(&mut Self) -> ()) {
        match self {
            Self::Tuple(ts) => ts.iter_mut().for_each(|t| f(&mut t.kind)),
            Self::Arrow(box l, box r) => {
                f(&mut l.kind);
                f(&mut r.kind);
            }
            Self::Struct(fields) => fields.values_mut().for_each(|t| f(&mut t.kind)),
            Self::TyVar(_) => f(self),
            Self::Infer(_) => f(self),
            Self::Bool | Self::F64 | Self::I64 | Self::Erased | Self::TyName(_) => {}
        }
    }

}

fn iter_ftv<'a, I, T>(iter: I) -> HashSet<u64> where T : 'a + Type, I : IntoIterator<Item = &'a T> {
    iter.into_iter()
        .map(|x| x.ftv())
        .fold(HashSet::new(), |acc, x| &acc | &x)
}

impl Type for TyKind {
    fn apply(&mut self, s: &Substitution) {
        match self {
            Self::Infer(i) => if let Some(t) = s.get(i) { *self = t.kind.clone() }
            Self::TyVar(_) => {}
            _ => self.apply_transformation(|ty| ty.apply(s))
        }
    }

    fn ftv(&self) -> HashSet<u64> {
        match self {
            Self::Infer(i) => set! { *i },
            // Doesn't seem to have a good way to union without copying the set in some way
            // But u64 copy cost isn't too bad anyhow
            Self::Tuple(xs)      => iter_ftv(xs),
            Self::Arrow(l, r)    => &l.ftv() | &r.ftv(),
            Self::Struct(fields) => iter_ftv(fields.values()),
            Self::Bool | Self::F64 | Self::I64 | Self::Erased | Self::TyVar(_) | Self::TyName(_) => HashSet::new(),
        }
    }
}

impl Display for TyKind {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Infer(i)         => write!(f, "τ{}", i),
            Self::I64              => write!(f, "i64"),
            Self::F64              => write!(f, "f64"),
            Self::Bool             => write!(f, "bool"),
            Self::Tuple(xs)        => write!(f, "({})", xs.iter().map(|x| x.to_string()).collect::<Vec<_>>().join(", ")),
            Self::TyVar(name)      => write!(f, "{}", name),
            Self::Erased           => write!(f, "τ"),
            Self::TyName(name)     => write!(f, "{}", name),
            Self::Struct(fields)   => write!(f, "{{ \n\t{} \n}}", fmt_iter_debug(fields, ";\n\t")),
            Self::Arrow(box l, r)  => match l.kind {
                Self::Arrow(..) => write!(f, "({}) -> {}", l, r),
                _               => write!(f, "{} -> {}", l, r),
            },
        }
    }
}


/// Convenience method, primarily for testing
impl TyKind { pub fn to_ty(self) -> Ty { Ty::new(Span::single(0, 0), self) } }

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_ftv() {
        let a = TyKind::Infer(0).to_ty();
        let b = TyKind::Tuple(vec![TyKind::F64.to_ty(), TyKind::Infer(2).to_ty()]).to_ty();
        let f = TyKind::Arrow(box a, box b);
        assert_eq!(f.ftv(), set! { 0, 2 })
    }

    #[test]
    fn test_lowering_ty_var() {
        let mut generator = Counter::new();

        let mut ty = arrow! { TyKind::TyVar("'a".to_owned()).to_ty() => TyKind::TyVar("'b".to_owned()).to_ty() };
        ty.lower_tyvar_to_uvar(&mut generator);
        assert_eq!(ty, arrow! { TyKind::Infer(1).to_ty() => TyKind::Infer(2).to_ty() });

        let mut ty = arrow! {
            TyKind::TyVar("'a".to_owned()).to_ty()
                => TyKind::TyVar("'b".to_owned()).to_ty()
                => TyKind::TyVar("'a".to_owned()).to_ty()
        };
        ty.lower_tyvar_to_uvar(&mut generator);
        assert_eq!(ty, arrow! { TyKind::Infer(3).to_ty() => TyKind::Infer(4).to_ty() => TyKind::Infer(3).to_ty() })

    }
}






