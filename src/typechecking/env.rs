use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::fmt::Debug;
use std::borrow::Borrow;
use super::{Type, TyScheme, Ty, Substitution};

#[derive(Debug, PartialEq)]
pub(crate) struct Env<K, V> where K : Hash + Eq {
    contexts: Vec<Ctx<K, V>>,
    saved: Vec<usize>,
}

impl<K, R> Type for Env<K, R> where K : Hash + Eq, R : Type {

    fn ftv(&self) -> HashSet<u64> {
        self.contexts.iter().map(|ctx| ctx.ftv()).fold(HashSet::new(), |acc, x| &acc | &x)
    }

    fn apply(&mut self, s: &Substitution) {
        self.contexts.iter_mut().for_each(|ctx| ctx.apply(s))
    }
}

impl<K, V> Extend<(K, V)> for Env<K, V> where K : Eq + Hash {
    fn extend<T : IntoIterator<Item = (K, V)>>(&mut self, iter: T) {
        self.peek_mut().extend(iter)
    }
}

impl<K, V> Extend<(K, Ty)> for Env<K, V> where K : Eq + Hash, V : From<TyScheme> + Type {
    fn extend<I>(&mut self, iter: I) where I : IntoIterator<Item = (K, Ty)> {
        let schemes = iter.into_iter()
            .map(|(k, ty)| (k, V::from(ty.generalize(self))))
            .collect::<Vec<_>>();
        self.peek_mut().extend(schemes);
    }
}

// impl<K> Extend<(K, Ty)> for Env<K, TyScheme> where K : Eq + Hash {
//     fn extend<T : IntoIterator<Item = (K, Ty)>>(&mut self, iter: T) {
//         let schemes = iter.into_iter().map(|(k, ty)| (k, ty.generalize(self))).collect::<Vec<_>>();
//         self.peek_mut().extend(schemes);
//     }
// }

impl<'a, K, V> Env<K, V> where K : Hash + Eq {

    pub fn new() -> Self {
        Self { contexts: vec![Ctx::new()], saved: vec![] }
    }

    pub fn define(&mut self, k: K, v: V) { self.peek_mut().insert(k, v) }
    pub fn peek(&self) -> &Ctx<K, V> { self.contexts.last().unwrap() }
    pub fn peek_mut(&mut self) -> &mut Ctx<K, V> { self.contexts.last_mut().unwrap() }

    /// Saves a scope as a restore point
    pub fn save(&mut self) { self.saved.push(self.contexts.len()) }

    /// Restore pops every scope after the saved one
    /// Saved scope is NOT removed
    pub fn restore(&mut self) { self.contexts.drain(self.saved.pop().unwrap()..); }

    pub fn push(&mut self) { self.contexts.push(Ctx::new()) }

    pub fn pop(&mut self) { self.contexts.pop(); }

    pub fn lookup<Q : ?Sized>(&self, k: &Q) -> Option<&V> where K: Borrow<Q>, Q: Hash + Eq {
        for ctx in self.contexts.iter().rev() {
            if let Some(v) = ctx.lookup(k) { return Some(v) }
        }
        None
    }

}

#[derive(Debug, PartialEq)]
pub(crate) struct Ctx<K, V> where K : Hash + Eq {
    ctx: HashMap<K, V>
}

impl<K, R> Type for Ctx<K, R> where K : Hash + Eq, R : Type {

    fn ftv(&self) -> HashSet<u64> {
        self.ctx.values().map(|t| t.ftv()).fold(HashSet::new(), |acc, x| &acc | &x)
    }

    fn apply(&mut self, s: &Substitution) {
        self.ctx.values_mut().for_each(|t| t.apply(s))
    }
}


impl<K, V> Extend<(K ,V)> for Ctx<K, V> where K : Eq + Hash {
    fn extend<T : IntoIterator<Item = (K, V)>>(&mut self, iter: T) {
        self.ctx.extend(iter)
    }
}

impl<K, V> Ctx<K, V> where K : Hash + Eq {

    pub fn new() -> Self {
        Self { ctx: HashMap::new() }
    }

    pub fn insert(&mut self, k: K, v: V) {
        self.ctx.insert(k, v);
    }

    pub fn lookup<Q: ?Sized>(&self, k: &Q) -> Option<&V> where K: Borrow<Q>, Q: Hash + Eq {
        self.ctx.get(k)
    }

}

impl<K, V> From<HashMap<K, V>> for Ctx<K, V> where K : Eq + Hash {
    fn from(map: HashMap<K, V>) -> Self {
        Self { ctx: map }
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_env() {
        let mut env = Env::new();
        env.push();
        env.define(0, 0);
        env.push();
        env.save();
        env.define(1, 1);
        env.push();
        env.define(100, 100);
        env.push();
        env.define(200, 200);
        env.restore();
        env.pop();
        env.pop();
        env.save();

        let mut expected = Env::new();
        expected.saved = vec![1];
        assert_eq!(env, expected);
    }
}












