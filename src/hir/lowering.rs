use bumpalo;
use crate::parsing::{Expr, ExprKind};
use crate::hir as hir;

struct LoweringCtx<'hir> {
    arena: &'hir bumpalo::Bump,
}

impl<'hir> LoweringCtx<'hir> {
    pub(super) fn lower_expr<'a, 'b>(&'a mut self, expr: &'b Expr) -> &'hir hir::Expr<'hir> {
        self.arena.alloc(self.lower_expr_mut(expr))
    }

    pub(super) fn lower_expr_mut<'a, 'b>(&'a mut self, expr: &'b Expr) -> hir::Expr<'hir> {
        let kind = match &expr.kind {
            ExprKind::Unary { op, box expr } => hir::ExprKind::Unary(*op, self.lower_expr(expr)),
            ExprKind::Integral { value } => hir::ExprKind::Int(*value),
            _ => unimplemented!()
        };

        hir::Expr::new(expr.span, kind)
    }
}

