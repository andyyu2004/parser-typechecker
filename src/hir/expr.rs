use crate::parsing::{Span, BinOp, UnaryOp};

pub(crate) struct Expr<'hir> {
    span: Span,
    kind: ExprKind<'hir>,
}

impl<'hir> Expr<'hir> {
    pub(crate) fn new(span: Span, kind: ExprKind<'hir>) -> Self {
        Self { span, kind }
    }
}

pub(crate) enum ExprKind<'hir> {
    Int(i64),
    Unary(UnaryOp, &'hir Expr<'hir>),
}

