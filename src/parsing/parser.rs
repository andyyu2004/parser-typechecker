use super::*;
use regexlexer::{Token, TokenKind};
use crate::error::Error;
use super::parselets::*;
use crate::typechecking::{Ty, TyKind};
use crate::util::{string, Counter};

pub struct Parser<'a> {
    tokens: &'a [Token<'a>],
    i: usize, // Current index inside tokens
    ty_var_gen: &'a mut Counter,
    id_gen: Counter,
    span_stack: Vec<usize>,
    backtrack_index: usize,
}

// Parser functions return a tuple of an exprkind as the fields of the expr can be filled out by the parser
// It also return a Option of a Type if there is a better option than just generating a new unification variable
type NullParseFn = for<'r, 'b> fn(&'r mut Parser<'b>, Token<'b>)       -> Result<(ExprKind, Option<Ty>), Error>;
type LeftParseFn = for<'r, 'b> fn(&'r mut Parser<'b>, Expr, Token<'b>) -> Result<(ExprKind, Option<Ty>), Error>;

impl<'a> Parser<'a> {
    pub fn new(tokens: &'a [Token<'a>], ty_var_gen: &'a mut Counter) -> Self {
        Parser { tokens, i: 0, backtrack_index: 0, span_stack: Vec::new(), ty_var_gen, id_gen: Counter::new() }
    }

    /// Returns the index into the src file the parser is currently at
    pub(crate) fn src_index(&self) -> usize { self.curr_or_last().index }
    pub(crate) fn src_line(&self) -> usize { self.curr_or_last().line }

    pub(crate) fn end_span(&mut self) -> Span { Span::new(self.span_stack.pop().unwrap(), self.src_index(), self.src_line()) }
    pub(crate) fn peek_span(&self) -> Span { Span::new(*self.span_stack.last().unwrap(), self.src_index(), self.src_line()) }
    pub(crate) fn get_single_span(&self) -> Span { Span::single(self.src_index(), self.src_line()) }

    pub(crate) fn gen_id(&mut self) -> u64 { self.id_gen.next() }

    pub(crate) fn gen_type_var(&mut self) -> Ty {
        Ty::new(Span::single(self.src_index(), self.src_line()), TyKind::Infer(self.ty_var_gen.next()))
    }

    // the driver function for parsing in expression mode
    pub fn run_parse_expr(&mut self) -> Result<Expr, Vec<Error>> {
        let expr = self.parse_expression(Precedence::ZERO).map_err(|err| vec![err])?;
        if self.peek().map(|x| x.kind) != Ok(TokenKind::EOF) {
            return Err(vec![Error::new(expr.span, format!("Did not consume all tokens (debug::currently on {:?})", self.peek()))]);
        }
        Ok(expr)
    }

    pub fn parse_program(&mut self) -> Result<Vec<Item>, Vec<Error>> {
        let mut items = vec![];
        let mut errors = vec![];
        while let Ok(tok) = self.peek() {
            if tok.kind == TokenKind::EOF { break }
            match self.parse_item() {
                Ok(item) => items.push(item),
                Err(err) => {
                    errors.push(err);
                    // synchronize parser on error until next item or eof
                    let item_keywords = [TokenKind::Fn, TokenKind::Use];
                    while let Ok(tok) = self.peek() {
                        if item_keywords.contains(&tok.kind) { break }
                        self.i += 1;
                    }
                },
            }
        }
        items.sort();
        if errors.is_empty() { Ok(items) } else { Err(errors) }
    }

    pub(crate) fn parse_item(&mut self) -> Result<Item, Error> {
        self.begin_span();
        if self.matches(TokenKind::Fn) { parse_function(self) }
        else if self.matches(TokenKind::Use) { parse_use(self) }
        else if self.matches(TokenKind::Struct) { parse_struct_decl(self) }
        else { Err(Error::invalid_item(self.end_span(), self.prev())) }
    }

    pub(crate) fn begin_span(&mut self) { self.span_stack.push(self.src_index()); }

    fn curr_precedence(&self) -> Precedence { self.peek().map(Precedence::of_left).unwrap_or(Precedence::ZERO) }

    pub(crate) fn parse_expression(&mut self, precedence: Precedence) -> Result<Expr, Error> {
        self.span_stack.push(self.src_index());
        let token = self.next()?;
        let null_parse_fn = Parser::get_null_denotation_rule(token.kind)
            .ok_or_else(|| Error::new(self.peek_span(), format!("Failed to parse null denotation token `{}`", token)))?;

        let (kind, ty) = null_parse_fn(self, token)?;
        let mut expr = self.mk_expr(kind, ty);

        // The left denotation lookup won't fail unlike the null denotation due to the precedence condition on the while loop
        // Bad operators will have zero precedence and hence never enter the loop
        while self.curr_precedence() > precedence {
            self.begin_span();
            let token = self.next()?;
            let left_parse_fn = Parser::get_left_denotation_rule(token.kind);
            let (kind, ty) = left_parse_fn(self, expr, token)?;
            expr = self.mk_expr(kind, ty);
        }
        Ok(expr)
    }

    fn mk_expr(&mut self, kind: ExprKind, ty: Option<Ty>) -> Expr {
        Expr::new(self.end_span(), kind, ty.unwrap_or_else(|| self.gen_type_var()), self.gen_id())
    }

    pub(crate) fn parse_type(&mut self) -> Result<Ty, Error> {
        self.begin_span();
        if self.matches(TokenKind::Bool) {
            Ok(Ty::new(self.end_span(), TyKind::Bool))
        } else if self.matches(TokenKind::Int) {
            Ok(Ty::new(self.end_span(), TyKind::I64))
        } else if self.matches(TokenKind::LParen) {
            self.set_backtrack();
            let ty = self.parse_type()?;
            // Parse single types within parens as a tuplew
            Ok(if self.matches(TokenKind::RParen) { ty } else {
                self.backtrack();
                let (types, span) = self.parse_tuple(Self::parse_type)?;
                Ty::new(span, TyKind::Tuple(types))
            })
        } else if self.matches(TokenKind::Fn) {
            self.expect(TokenKind::LParen)?;
            let (l, span) = self.parse_tuple(Self::parse_type)?;
            let ttuple = box Ty::new(span, TyKind::Tuple(l));
            self.expect(TokenKind::RArrow)?;
            let r = box self.parse_type()?;
            let kind = TyKind::Arrow(ttuple, r);
            Ok(Ty::new(self.end_span(), kind))
        } else if self.matches(TokenKind::Typevar) {
            Ok(Ty::new(self.end_span(), TyKind::TyVar(self.prev().lexeme.to_owned())))
        } else if self.matches(TokenKind::Typename) {
            Ok(Ty::new(self.end_span(), TyKind::TyName(self.prev().lexeme.to_owned())))
        } else {
            unimplemented!();
        }
    }

    pub(crate) fn parse_ty_params(&mut self) -> Result<Vec<String>, Error> {
        let mut ty_params = vec![];
        if self.matches(TokenKind::GT) { return Ok(ty_params) }
        while {
            self.begin_span();
            let ty_var = self.expect(TokenKind::Typevar)?.lexeme.to_owned();
            ty_params.push(ty_var);
            self.matches(TokenKind::Comma)
        }{}
        Ok(ty_params)
    }

    pub(crate) fn parse_tuple<T>(&mut self, parse_fn: impl Fn(&mut Parser<'a>) -> Result<T, Error>) -> Result<(Vec<T>, Span), Error> {
        self.span_stack.push(self.src_index());
        let mut vec = vec![];
        while !self.matches(TokenKind::RParen) {
            vec.push(parse_fn(self)?);
            if !self.matches(TokenKind::Comma) {
                self.expect(TokenKind::RParen)?;
                break;
            }
        }
        Ok((vec, self.end_span()))
    }

    pub(crate) fn parse_optionally_typed_binder(&mut self) -> Result<Binder, Error> { self.parse_binder(false) }

    pub(crate) fn parse_binder(&mut self, require_annotations: bool) -> Result<Binder, Error> {
        self.begin_span();
        let name = self.expect(TokenKind::Identifier)?.lexeme.to_owned();
        let ty = if self.matches(TokenKind::Colon) { self.parse_type()? }
        else if !require_annotations { self.gen_type_var() }
        else { return Err(Error::cannot_elide_item_type_annotations(self.end_span(), &name)) };
        Ok(Binder::new(self.end_span(), name, ty))
    }

    /// Returns the relevant null denotation parse function for the tokenkind
    fn get_null_denotation_rule(token_kind: TokenKind) -> Option<NullParseFn> {
        match token_kind {
            TokenKind::Integral   => Some(parse_integral),
            TokenKind::LParen     => Some(parse_group),
            TokenKind::Identifier => Some(parse_id),
            TokenKind::Str        => Some(parse_str),
            TokenKind::Let        => Some(parse_let),
            TokenKind::Var        => Some(parse_var),
            TokenKind::LBrace     => Some(parse_block),
            TokenKind::Fn         => Some(parse_lambda),
            TokenKind::If         => Some(parse_if),
            TokenKind::While      => Some(parse_while),
            TokenKind::Typename   => Some(parse_data),
            TokenKind::False | TokenKind::True => Some(parse_bool),
            TokenKind::Plus | TokenKind::Minus | TokenKind::Tilde | TokenKind::Bang => Some(parse_prefix_op),
            _ => None
        }
    }

    fn get_left_denotation_rule(token_kind: TokenKind) -> LeftParseFn {
        match token_kind {
            TokenKind::Equal  => parse_assignment,
            TokenKind::LParen => parse_application,
            TokenKind::Dot    => parse_get,
            TokenKind::Plus
                | TokenKind::Minus
                | TokenKind::Slash
                | TokenKind::Star
                | TokenKind::LT
                | TokenKind::LTE
                | TokenKind::GT
                | TokenKind::GTE
                | TokenKind::DEqual
                | TokenKind::Ampersand
                | TokenKind::DAmpersand
                | TokenKind::Pipe
                | TokenKind::DPipe
                | TokenKind::BangEqual
                | TokenKind::DStar => parse_binary,
            _ => unimplemented!()
        }
    }

    /// Returns ref to current token and pushes the index forward if the peek is succesful
    pub(crate) fn next(&mut self) -> Result<Token<'a>, Error> {
        self.peek().map(|tok| { self.i += 1; tok })
    }

    pub(crate) fn set_backtrack(&mut self) { self.backtrack_index = self.i }
    pub(crate) fn backtrack(&mut self) { self.i = self.backtrack_index }

    /// Returns ref to current token or an error if the current token is at EOF or even further
    fn peek(&self) -> Result<Token<'a>, Error> {
        if self.i < self.tokens.len() {
            Ok(self.tokens[self.i])
        } else {
            Err(Error::new(Span::single(self.src_index(), self.src_line()), "Ran out of tokens".to_owned()))
        }
    }

    /// Asserts the next token is the one given;
    pub(crate) fn expect(&mut self, kind: TokenKind) -> Result<Token<'a>, Error> {
        self.expect_without_advance(kind).map(|k| { self.i += 1; k })
    }

    pub(crate) fn expect_one_of(&mut self, kinds: &[TokenKind]) -> Result<Token<'a>, Error> {
        self.expect_one_of_without_advance(kinds).map(|k| { self.i += 1; k })
    }

    pub(crate) fn expect_one_of_without_advance(&mut self, kinds: &[TokenKind]) -> Result<Token<'a>, Error> {
        let curr = self.peek()?;
        if kinds.contains(&curr.kind) { Ok(curr) }
        else { Err(Error::new(Span::single(self.src_index(), self.src_line()), format!("Expected `{}` found `{}`", string::join(kinds, ", "), curr.kind))) }
    }

    pub(crate) fn expect_without_advance(&mut self, kind: TokenKind) -> Result<Token<'a>, Error> {
        self.expect_one_of_without_advance(&[kind])
    }

    /// Returns a boolean indicating whether the next token matches the one provided;
    /// If so, consumes the token;
    pub(crate) fn matches(&mut self, kind: TokenKind) -> bool {
        let is_match = self.peek().map(|t| t.kind) == Ok(kind);
        if is_match { self.i += 1 };
        is_match
    }

    fn prev(&self) -> Token<'a> {
        // assume input is valid and hence non empty
        if self.i > 1 { self.tokens[self.i - 1] } else { self.tokens[self.i] }
    }

    /// Convenience method for grabbing a token for error handling purposes
    fn curr_or_last(&self) -> Token {
        if self.i < self.tokens.len() { self.tokens[self.i] }
        else { *self.tokens.last().unwrap() }
    }
}








