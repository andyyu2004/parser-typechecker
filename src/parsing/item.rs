use super::{Span, Expr, Binder, fmt_iter, fmt_iter_debug};
use crate::typechecking::Ty;
use std::fmt::{self, Debug, Formatter, Display};
use std::collections::HashMap;
use std::cmp::Ordering;

#[derive(Eq, PartialEq)]
pub struct Item {
    pub name: String,
    pub span: Span,
    pub node_id: u64,
    pub ty: Ty,
    pub kind: ItemKind,
    pub ty_params: Vec<String>,
}

impl PartialOrd for Item {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.kind.partial_cmp(&other.kind)
    }
}

impl Ord for Item {
    fn cmp(&self, other: &Self) -> Ordering {
        self.kind.cmp(&other.kind)
    }
}


impl Item {
    pub fn new(name: String, ty_params: Vec<String>, span: Span, kind: ItemKind, ty: Ty, node_id: u64) -> Self {
        Self { name, span, kind, ty, node_id, ty_params }
    }
}

impl Debug for Item {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "item {}<{}> {:?}", self.name, fmt_iter(&self.ty_params, ", "), self.kind)
    }
}

type Path = String;

#[derive(Eq, PartialEq)]
pub enum ItemKind {
    Fn { params: Vec<Binder>, ret_ty: Ty, body: Expr },
    Struct { fields: HashMap<String, Ty> },
    Use { path: Path }
}

/// we define an ordering on itemkinds such that the parser can sort them regardless of their real order of definition in the src file
/// we generally wish to process items in a certain order to minimize unbound names
/// e.g. define struct before the fn in case the fn takes the struct as a parameter or returns one
/// similarly, do all imports before processing any other item
/// less => process first

impl PartialOrd for ItemKind {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for ItemKind {
    fn cmp(&self, other: &Self) -> Ordering {
        self.as_num().cmp(&other.as_num())
    }
}

impl ItemKind {

    /// assigns a number to each variant to help with implementing an ordering
    pub fn as_num(&self) -> u8 {
        match self {
            Self::Use { .. }    => 0,
            Self::Struct { .. } => 50,
            Self::Fn { .. }     => 100,
        }
    }

    pub fn is_fn(&self) -> bool {
        match self {
            Self::Fn { .. } => true,
            _               => false,
        }
    }

    pub fn is_use(&self) -> bool {
        match self {
            Self::Use { .. } => true,
            _               => false,
        }
    }
}

impl Display for ItemKind {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Fn { params, ret_ty, body } => writeln!(f, "fn ({}) -> {} {}", fmt_iter(params, ", "), ret_ty, body),
            Self::Struct { fields }           => writeln!(f, "struct {}", fmt_iter_debug(fields, ";\n")),
            Self::Use { path }                => writeln!(f, "use {}", path),
        }
    }
}

impl Debug for ItemKind {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}


#[cfg(test)]
mod test {
    use super::*;

    fn test_itemkind_ordering() {
        assert!(ItemKind::Use { path: "sdf".to_owned() } < ItemKind::Struct { fields: HashMap::new() });
    }
}
