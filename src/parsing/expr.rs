use regexlexer::TokenKind;
use std::fmt::{self, Display, Formatter, Debug, Error};
use crate::typechecking::Ty;
use crate::parsing::Span;
use std::collections::HashMap;

#[derive(PartialEq, Clone, Eq)]
pub struct Expr {
    pub kind: ExprKind,
    pub span: Span,
    pub ty: Ty,
    pub node_id: u64,
}

impl Expr {
    pub fn new(span: Span, kind: ExprKind, ty: Ty, node_id: u64) -> Self {
        Expr { span, kind, ty, node_id }
    }

    pub fn is_lvalue(&self) -> bool {
        match self.kind {
            ExprKind::Id { .. } => true,
            _                   => false,
        }
    }
}

impl Display for Expr {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result { write!(f, "{}", self.kind) }
}

impl Debug for Expr {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result { write!(f, "{:?}", self.kind) }
}

#[derive(Clone, PartialEq, Eq)]
pub struct Binder {
    pub span: Span,
    pub name: String,
    pub ty: Ty,
}

impl Binder {
    pub fn new(span: Span, name: String, ty: Ty) -> Self {
        Self { span, name, ty }
    }
}

impl Display for Binder {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}: {}", self.name, self.ty)
    }
}

impl Debug for Binder { fn fmt(&self, f: &mut Formatter) -> fmt::Result { write!(f, "{}", self) } }

#[derive(Clone, PartialEq, Copy, Eq)]
pub enum BinOp { NEq, Eq, LT, LTE, GT, GTE, BitOr, BitAnd, Add, Sub, Mul, Div }

impl Display for BinOp {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", TokenKind::from(*self))
    }
}

impl From<TokenKind> for BinOp {
    fn from(kind: TokenKind) -> Self {
        match kind {
            TokenKind::BangEqual  => Self::NEq,
            TokenKind::Equal      => Self::Eq,
            TokenKind::LT         => Self::LT,
            TokenKind::GT         => Self::GT,
            TokenKind::LTE        => Self::LTE,
            TokenKind::GTE        => Self::GTE,
            TokenKind::Ampersand  => Self::BitAnd,
            TokenKind::Pipe       => Self::BitOr,
            TokenKind::Plus       => Self::Add,
            TokenKind::Minus      => Self::Sub,
            TokenKind::Star       => Self::Mul,
            TokenKind::Slash      => Self::Div,
            _ => panic!("`{}` is not a valid unary op", kind)
        }
    }
}

impl From<BinOp> for TokenKind {
    fn from(op: BinOp) -> Self {
        match op {
            BinOp::NEq    => Self::BangEqual,
            BinOp::Eq     => Self::Equal,
            BinOp::LT     => Self::LT,
            BinOp::GT     => Self::GT,
            BinOp::LTE    => Self::LTE,
            BinOp::GTE    => Self::GTE,
            BinOp::BitAnd => Self::Ampersand,
            BinOp::BitOr  => Self::Pipe,
            BinOp::Add    => Self::Plus,
            BinOp::Sub    => Self::Minus,
            BinOp::Mul    => Self::Star,
            BinOp::Div    => Self::Slash,
        }
    }
}

#[derive(Clone, PartialEq, Copy, Eq)]
pub enum UnaryOp { Not, Negate }

impl From<TokenKind> for UnaryOp {
    fn from(kind: TokenKind) -> Self {
        match kind {
            TokenKind::Bang  => Self::Not,
            TokenKind::Minus => Self::Negate,
            _ => panic!("`{}` is not a valid unary op", kind)
        }
    }
}

impl From<UnaryOp> for TokenKind {
    fn from(op: UnaryOp) -> Self {
        match op {
            UnaryOp::Not     => Self::Bang,
            UnaryOp::Negate  => Self::Minus,
        }
    }
}

impl Display for UnaryOp {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", TokenKind::from(*self))
    }
}

#[derive(Clone, PartialEq, Copy, Eq)]
pub enum LazyOp { And, Or }

impl From<TokenKind> for LazyOp {
    fn from(kind: TokenKind) -> Self {
        match kind {
            TokenKind::DAmpersand  => Self::And,
            TokenKind::DPipe       => Self::Or,
            _ => panic!("`{}` is not a valid lazy op", kind)
        }
    }
}

impl From<LazyOp> for TokenKind {
    fn from(op: LazyOp) -> Self {
        match op {
            LazyOp::Or  => Self::DPipe,
            LazyOp::And => Self::DAmpersand,
        }
    }
}

impl Display for LazyOp {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", TokenKind::from(*self))
    }
}

#[derive(Clone, PartialEq, Eq)]
pub enum ExprKind {
    If { cond: Box<Expr>, left: Box<Expr>, right: Option<Box<Expr>> },
    Unary { op: UnaryOp, expr: Box<Expr> },
    Integral { value: i64 },
    Bool { b: bool },
    Id { name: String },
    Str { string: String },
    Binary { op: BinOp, left: Box<Expr>, right: Box<Expr> },
    Grouping { expr: Box<Expr> },
    Let { binder: Binder, bound: Box<Expr>, mutable: bool },
    Block { exprs: Vec<Expr>, suppressed: bool },
    Lambda { params: Vec<Binder>, ret: Ty, body: Box<Expr> },
    App { f: Box<Expr>, args: Vec<Expr> },
    Tuple { elems: Vec<Expr> },
    Lazy { op: LazyOp, left: Box<Expr>, right: Box<Expr> },
    While { cond: Box<Expr>, body: Box<Expr> },
    Assn { name: String, expr: Box<Expr> },
    Struct { name: String, fields: HashMap<String, Expr> },
    Get { expr: Box<Expr>, field: String },
}

pub fn fmt_iter<T>(xs: impl IntoIterator<Item = T>, sep: &str) -> String where T : Display {
    xs.into_iter().map(|x| x.to_string()).collect::<Vec<_>>().join(sep)
}

pub fn fmt_iter_debug<T>(xs: impl IntoIterator<Item = T>, sep: &str) -> String where T : Debug {
    xs.into_iter().map(|x| format!("{:?}", x)).collect::<Vec<_>>().join(sep)
}

impl Display for ExprKind {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match self {
            Self::Unary { op, expr }             => write!(fmt, "{}{}", op, expr),
            Self::Integral { value }             => write!(fmt, "{}", value),
            Self::Id { name }                    => write!(fmt, "{}", name),
            Self::Binary { op, left, right }     => write!(fmt, "{} {} {}", left, op, right),
            Self::Lazy { op, left, right }       => write!(fmt, "{} {} {}", left, op, right),
            Self::Grouping { expr }              => write!(fmt, "({})", expr),
            Self::Bool { b }                     => write!(fmt, "{}", b),
            Self::Str { string }                 => write!(fmt, "{}", string),
            Self::Let { binder, bound, mutable } => write!(fmt, "{} {} = {}", if *mutable { "var" } else { "let" }, binder, bound),
            Self::Block { exprs, suppressed }    => write!(fmt, "{{ {}{} }}", fmt_iter(exprs, "; "), if *suppressed { ";" } else {""}),
            Self::Lambda { params, ret, body }   => write!(fmt, "fn ({}) -> {} => {}", fmt_iter(params, ", "), ret, body),
            Self::App { f, args }                => write!(fmt, "{}({})", f, fmt_iter(args, ", ")),
            Self::Tuple { elems }                => write!(fmt, "({})", fmt_iter(elems, ", ")),
            Self::While { cond, body }           => write!(fmt, "while {} {}", cond, body),
            Self::Assn { name, expr }            => write!(fmt, "{} <- {}", name, expr), // formatting using <- to make it very clear its assignment
            Self::Struct { name, fields }        => write!(fmt, "{} {{ {} }}", name, fmt_iter_debug(fields, ", ")),
            Self::Get { expr, field }            => write!(fmt, "{}.{}", expr, field),
            Self::If { cond, left, right }       => match right {
                Some(right) => write!(fmt, "if {} {} else {}", cond, left, right),
                None        => write!(fmt, "if {} {}", cond, left),
            }
        }
    }
}

impl Debug for ExprKind {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match self {
            Self::Unary { op, expr }             => write!(fmt, "({}{:?})", op, expr),
            Self::Integral { value }             => write!(fmt, "{}", value),
            Self::Id { name }                    => write!(fmt, "{}", name),
            Self::Binary { op, left, right }     => write!(fmt, "({} {:?} {:?})", op, left, right),
            Self::Lazy { op, left, right }       => write!(fmt, "({} {:?} {:?})", op, left, right),
            Self::Grouping { expr }              => write!(fmt, "{:?}", expr),
            Self::Bool { b }                     => write!(fmt, "{}", b),
            Self::Str { string }                 => write!(fmt, "{}", string),
            Self::Let { binder, bound, mutable } => write!(fmt, "({} [{} = {:?}])", if *mutable { "var" } else { "let" }, binder, bound),
            Self::Block { exprs, suppressed }    => write!(fmt, "{{ {}{} }}", fmt_iter_debug(exprs, "; "), if *suppressed { ";" } else {""}),
            Self::Lambda { params, ret, body }   => write!(fmt, "(lambda ({}) -> {} => {:?})", fmt_iter_debug(params, ", "), ret, body),
            Self::App { f, args }                => write!(fmt, "({:?} {})", f, fmt_iter_debug(args, " ")),
            Self::Tuple { elems }                => write!(fmt, "({})", fmt_iter_debug(elems, ", ")),
            Self::While { cond, body }           => write!(fmt, "(while {:?} {:?})", cond, body),
            Self::Assn { name, expr }            => write!(fmt, "({} <- {})", name, expr),
            Self::Struct { name, fields }        => write!(fmt, "({} {{ {} }})", name, fmt_iter_debug(fields, ", ")),
            Self::Get { expr, field }            => write!(fmt, "[{} {}]", expr, field),
            Self::If { cond, left, right }       => match right {
                Some(right) => write!(fmt, "(if ({:?}) {:?} {:?})", cond, left, right),
                None        => write!(fmt, "(if ({:?}) {:?})", cond, left),
            }
        }
    }

}



















