use crate::parsing::{Parser, Item, ItemKind, Expr};
use crate::typechecking::{Ty, TyKind};
use super::super::parse_block;
use regexlexer::TokenKind;
use crate::error::Error;

pub(crate) fn parse_function(parser: &mut Parser) -> Result<Item, Error> {
    let name = parser.expect(TokenKind::Identifier)?.lexeme.to_owned();
    let ty_params = if parser.matches(TokenKind::LT) { parser.parse_ty_params()? } else { vec![] };
    parser.expect(TokenKind::LParen)?;
    let mut params = vec![];

    while !parser.matches(TokenKind::RParen) {
        let binder = parser.parse_binder(true)?;
        params.push(binder);
        if !parser.matches(TokenKind::Comma) {
            parser.expect(TokenKind::RParen)?;
            break;
        }
    }

    let ret_ty = if parser.matches(TokenKind::RArrow) { parser.parse_type()? } else { Ty::new(parser.peek_span(), TyKind::unit()) };

    let token = parser.expect(TokenKind::LBrace)?;
    parser.begin_span(); // begin span for block
    let (exprkind, ty) = parse_block(parser, token)?;
    let body = Expr::new(parser.end_span(), exprkind, ty.unwrap_or(parser.gen_type_var()), parser.gen_id());

    let itemkind = ItemKind::Fn { params, body, ret_ty };
    Ok(Item::new(name, ty_params, parser.end_span(), itemkind, parser.gen_type_var(), parser.gen_id()))
}
