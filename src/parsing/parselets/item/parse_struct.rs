use crate::parsing::{Parser, Item, ItemKind, Binder};
use crate::typechecking::{Ty, TyKind};
use regexlexer::TokenKind;
use crate::error::Error;
use std::collections::HashMap;

pub(crate) fn parse_struct_decl(parser: &mut Parser) -> Result<Item, Error> {
    let name = parser.expect(TokenKind::Typename)?.lexeme.to_owned();
    let ty_params = if parser.matches(TokenKind::LT) { parser.parse_ty_params()? } else { vec![] };
    let mut fields = HashMap::new();
    if parser.matches(TokenKind::LBrace) {
        while !parser.matches(TokenKind::RBrace) {
            let Binder { name, ty, .. } = parser.parse_binder(true)?;
            fields.insert(name, ty);
            if !parser.matches(TokenKind::SemiColon) {
                parser.expect(TokenKind::RBrace)?;
                break;
            }
        }
    }
    let kind = ItemKind::Struct { fields };
    let ty = Ty::new(parser.peek_span(), TyKind::TyName(name.clone()));
    Ok(Item::new(name, ty_params, parser.end_span(), kind, ty, parser.gen_id()))
}


