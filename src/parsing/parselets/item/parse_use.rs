use crate::parsing::{Parser, Item, ItemKind};
use regexlexer::TokenKind;
use crate::error::Error;

pub(crate) fn parse_use(parser: &mut Parser) -> Result<Item, Error> {
    let path = parser.expect(TokenKind::Str)?.lexeme.to_owned();
    Ok(Item::new("".to_owned(), vec![], parser.end_span(), ItemKind::Use { path }, parser.gen_type_var(), parser.gen_id()))
}
