use crate::parsing::{Parser, ExprKind, Precedence};
use regexlexer::{Token, TokenKind};
use crate::error::Error;
use crate::typechecking::Ty;

pub(crate) fn parse_while(parser: &mut Parser, _token: Token) -> Result<(ExprKind, Option<Ty>), Error> {
    let cond = box parser.parse_expression(Precedence::ZERO)?;
    parser.expect_without_advance(TokenKind::LBrace)?; // body expr must be a block
    let body = box parser.parse_expression(Precedence::ZERO)?;
    let kind = ExprKind::While { cond, body };
    Ok((kind, None))
}
