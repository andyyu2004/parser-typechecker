use crate::parsing::{Parser, ExprKind, Precedence};
use regexlexer::{Token, TokenKind};
use crate::error::Error;
use crate::typechecking::Ty;

pub(crate) fn parse_lambda(parser: &mut Parser, _token: Token) -> Result<(ExprKind, Option<Ty>), Error> {
    // Allows no paren for single argument lambda
    let params = if !parser.matches(TokenKind::LParen) {
        vec![parser.parse_binder(false)?]
    } else { parser.parse_tuple(Parser::parse_optionally_typed_binder)?.0 };

    let ret = if parser.matches(TokenKind::RArrow) { Some(parser.parse_type()?) } else { None };

    parser.expect(TokenKind::RFArrow)?;
    let body = box parser.parse_expression(Precedence::ZERO)?;
    let ret = ret.unwrap_or_else(|| body.ty.clone()); // if no annotated return type just give it the ty of the body
    let kind = ExprKind::Lambda { params, ret, body };
    Ok((kind, None))
}
