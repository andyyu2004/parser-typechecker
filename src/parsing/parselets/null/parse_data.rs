use crate::parsing::{Parser, ExprKind, Precedence};
use crate::typechecking::{Ty, TyKind};
use regexlexer::{Token, TokenKind};
use crate::error::Error;
use std::collections::HashMap;

/// parse enum or struct
pub(crate) fn parse_data<'a>(parser: &mut Parser<'a>, token: Token<'a>) -> Result<(ExprKind, Option<Ty>), Error> {
    parser.expect_one_of_without_advance(&[TokenKind::LBrace, TokenKind::LParen])?;
    if parser.matches(TokenKind::LBrace) { parse_struct(parser, token) }
    else if parser.matches(TokenKind::LParen) { parse_struct(parser, token) }
    else { unreachable!() }
}

pub(crate) fn parse_struct<'a>(parser: &mut Parser<'a>, token: Token<'a>) -> Result<(ExprKind, Option<Ty>), Error> {
    let name = token.lexeme.to_owned();
    let ty_span = parser.peek_span();
    let mut fields = HashMap::new();
    while !parser.matches(TokenKind::RBrace) {
        let name = parser.expect(TokenKind::Identifier)?.lexeme.to_owned();
        parser.expect(TokenKind::Colon)?;
        let value = parser.parse_expression(Precedence::ZERO)?;
        fields.insert(name, value);
        if !parser.matches(TokenKind::Comma) {
            parser.expect(TokenKind::RBrace)?;
            break;
        }
    }
    let kind = ExprKind::Struct { name: name.clone(), fields };
    Ok((kind, Some(Ty::new(ty_span, TyKind::TyName(name)))))
}
