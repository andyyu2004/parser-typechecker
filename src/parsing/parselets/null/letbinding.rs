use crate::parsing::{Parser, ExprKind, Precedence};
use crate::typechecking::Ty;
use regexlexer::{Token, TokenKind};
use crate::error::Error;

pub(crate) fn parse_let<'a>(parser: &mut Parser<'a>, _token: Token<'a>) -> Result<(ExprKind, Option<Ty>), Error> {
    parse_var_decl(parser, _token, false)
}

pub(crate) fn parse_var<'a>(parser: &mut Parser<'a>, _token: Token<'a>) -> Result<(ExprKind, Option<Ty>), Error> {
    parse_var_decl(parser, _token, true)
}

fn parse_var_decl<'a>(parser: &mut Parser<'a>, _token: Token<'a>, mutable: bool) -> Result<(ExprKind, Option<Ty>), Error> {
    let binder = parser.parse_binder(false)?;
    parser.expect(TokenKind::Equal)?;
    let bound = box parser.parse_expression(Precedence::ZERO)?;
    let kind = ExprKind::Let { binder, bound, mutable };
    Ok((kind, None))
}


