
use crate::parsing::{Parser, ExprKind, Precedence};
use regexlexer::{Token, TokenKind};
use crate::error::Error;
use crate::typechecking::Ty;

// the expect without advances are to allow only certain expressions
// specifically: only block after if; block or if after else
pub(crate) fn parse_if<'a>(parser: &mut Parser<'a>, _token: Token<'a>) -> Result<(ExprKind, Option<Ty>), Error> {
    let cond = box parser.parse_expression(Precedence::ZERO)?;
    parser.expect_without_advance(TokenKind::LBrace)?;
    let left = box parser.parse_expression(Precedence::ZERO)?;
    let right = if parser.matches(TokenKind::Else) {
        parser.expect_one_of_without_advance(&[TokenKind::LBrace, TokenKind::If])?;
        Some(box parser.parse_expression(Precedence::ZERO)?)
    } else { None };

    let kind = ExprKind::If { cond, left, right };
    Ok((kind, None))
}
