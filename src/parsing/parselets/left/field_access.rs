use crate::parsing::{Parser, Expr, ExprKind};
use regexlexer::{Token, TokenKind};
use crate::error::Error;
use crate::typechecking::Ty;

pub(crate) fn parse_get<'a>(parser: &mut Parser<'a>, left: Expr, _token: Token<'a>) -> Result<(ExprKind, Option<Ty>), Error> {
    let field = parser.expect(TokenKind::Identifier)?.lexeme.to_owned();
    let kind = ExprKind::Get { expr: box left, field };
    Ok((kind, None))
}
