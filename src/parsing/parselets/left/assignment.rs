use crate::parsing::{Parser, Expr, Precedence, ExprKind};
use regexlexer::Token;
use crate::error::Error;
use crate::typechecking::Ty;

pub(crate) fn parse_assignment<'a>(parser: &mut Parser<'a>, target: Expr, _token: Token<'a>) -> Result<(ExprKind, Option<Ty>), Error> {
    let expr = box parser.parse_expression(Precedence::ASSIGN - 1)?; // as right associative
    if let ExprKind::Id { name } = target.kind {
        let ty = expr.ty.clone();
        let kind = ExprKind::Assn { name, expr };
        Ok((kind, None))
    } else { Err(Error::expected_lvalue(&target)) }
}
